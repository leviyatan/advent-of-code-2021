#include <iostream>
#include <fstream>
#include <string>
#include <vector>

void read_file(std::vector<int> &input_list){
	std::ifstream input_file("input.txt");
	std::string str;

	while (std::getline(input_file, str)){
		input_list.push_back(std::stoi(str));
	}
	input_file.close();
}


int main(){
	std::vector<int> input_list;
	std::vector<int> sliding_window;

	read_file(input_list);
	int depth_measurement_increase = 0;

	for (int i = 0; i < input_list.size(); i++){
		if (i + 3 > input_list.size()) break;
		sliding_window.push_back(input_list[i] + input_list[i+1] + input_list[i+2]);
	}

	for (int i = 1; i < sliding_window.size(); i++){
		if (sliding_window[i] > sliding_window[i-1]) depth_measurement_increase++;
	}

	std::cout << depth_measurement_increase << std::endl;

	return 0;
}
