#include <iostream>
#include <fstream>
#include <vector>

void read_file(std::vector<std::string> &input_list){
	std::ifstream input_file("input.txt");
	std::string str;

	while (std::getline(input_file, str)){
		input_list.push_back(str);
	}
	input_file.close();
}


int main(){
	std::vector<std::string> input_list;
    int horizontal_position = 0;
    int depth = 0;

	read_file(input_list);

    for (int i = 0; i < input_list.size(); i++){
        char stringparse[input_list[i].length()];
        int intparse;
        sscanf(input_list[i].c_str(), "%s %d", &stringparse, &intparse);
        std::string direction(stringparse);
        if (direction == "forward") horizontal_position += intparse;
        if (direction == "down") depth += intparse;
        if (direction == "up") depth -= intparse;
    }

    std::cout << (horizontal_position * depth) << std::endl;
    
	return 0;
}
